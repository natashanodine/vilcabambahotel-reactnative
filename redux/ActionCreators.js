import * as ActionTypes from './ActionTypes';
import { baseUrl } from '../shared/baseUrl';

// COMMENTS
export const fetchComments = () => (dispatch) => {
    return fetch(baseUrl + 'comments')
        .then(response => {
            if (response.ok) {
                return response;
            } else {
                var error = new Error('Error ' + response.status + ': ' + response.statusText);
                error.response = response;
                throw error;
            }
        }, error => {
            var errMess = new Error(error.message);
            throw errMess;
        })
        .then(response => response.json())
        .then(comments => dispatch(addComments(comments)))
        .catch(error => dispatch(commentsFailed(error.message)))
};

export const commentsFailed = (errMess) => ({
    type: ActionTypes.COMMENTS_FAILED,
    payload: errMess
});

export const addComments = (comments) => ({
    type: ActionTypes.ADD_COMMENTS,
    payload: comments
});

export const postComment = (rentalId, rating, author, comment) => (dispatch) => {

    const newComment = {
        rentalId: rentalId,
        rating: rating,
        author: author,
        comment: comment
    }
    newComment.date = new Date().toISOString();
    return fetch(baseUrl + 'comments', {
        method: 'POST',
        body: JSON.stringify(newComment),
        headers: {
            'Content-Type': 'application/json'
        },
        credentials: 'same-origin'
    })
        .then(response => {
            if (response.ok) {
                return response;
            } else {
                var error = new Error('Error ' + response.status + ': ' + response.statusText);
                error.response = response;
                throw error;
            }
        },
            error => {
                var errmess = new Error(error.message);
                throw errmess;
            })
        .then(response => response.json())
        .then(response => setTimeout(() => {
            dispatch(addComment(response));
        }, 2000))
        .catch(error => {
            console.log('Post Comments ', error.message)
            alert('Your comment could not be posted\nError: ' + error.message)
        });
}

export const addComment = (comment) => ({
    type: ActionTypes.ADD_COMMENT,
    payload: comment
});

// RENTALS
export const fetchRentals = () => (dispatch) => {
    dispatch(rentalsLoading());
    return fetch(baseUrl + 'rentals')
        .then(response => {
            if (response.ok) {
                return response;
            } else {
                var error = new Error('Error ' + response.status + ': ' + response.statusText);
                error.response = response;
                throw error;
            }
        }, error => {
            var errMess = new Error(error.message);
            throw errMess;
        })
        .then(response => response.json())
        .then(rentals => dispatch(addRentals(rentals)))
        .catch(error => dispatch(rentalsFailed(error.message)))
};

export const rentalsLoading = () => ({
    type: ActionTypes.RENTALS_LOADING
});

export const rentalsFailed = (errMess) => ({
    type: ActionTypes.RENTALS_FAILED,
    payload: errMess
});

export const addRentals = (rentals) => ({
    type: ActionTypes.ADD_RENTALS,
    payload: rentals
});


// PROMOTIONS
export const fetchPromos = () => (dispatch) => {
    dispatch(promosLoading());
    return fetch(baseUrl + 'promotions')
        .then(response => {
            if (response.ok) {
                return response;
            } else {
                var error = new Error('Error ' + response.status + ': ' + response.statusText);
                error.response = response;
                throw error;
            }
        }, error => {
            var errMess = new Error(error.message);
            throw errMess;
        })
        .then(response => response.json())
        .then(promos => dispatch(addPromos(promos)))
        .catch(error => dispatch(promosFailed(error.message)))
};

export const promosLoading = () => ({
    type: ActionTypes.PROMOS_LOADING
});

export const promosFailed = (errMess) => ({
    type: ActionTypes.PROMOS_FAILED,
    payload: errMess
});

export const addPromos = (promos) => ({
    type: ActionTypes.ADD_PROMOS,
    payload: promos
});


// ACTIVITIES
export const fetchActivities = () => (dispatch) => {
    dispatch(activitiesLoading());
    return fetch(baseUrl + 'activities')
        .then(response => {
            if (response.ok) {
                return response;
            } else {
                var error = new Error('Error ' + response.status + ': ' + response.statusText);
                error.response = response;
                throw error;
            }
        }, error => {
            var errMess = new Error(error.message);
            throw errMess;
        })
        .then(response => response.json())
        .then(activities => dispatch(addActivities(activities)))
        .catch(error => dispatch(activitiesFailed(error.message)))
};

export const activitiesLoading = () => ({
    type: ActionTypes.ACTIVITIES_LOADING
});

export const activitiesFailed = (errMess) => ({
    type: ActionTypes.ACTIVITIES_FAILED,
    payload: errMess
});

export const addActivities = (activities) => ({
    type: ActionTypes.ADD_ACTIVITIES,
    payload: activities
});

export const postFavorite = (rentalId) => (dispatch) => {
    setTimeout(() => {
        dispatch(addFavorite(rentalId));
    }, 2000);
};

export const addFavorite = (rentalId) => ({
    type: ActionTypes.ADD_FAVORITE,
    payload: rentalId
});

export const deleteFavorite = (rentalId) => ({
    type: ActionTypes.DELETE_FAVORITE,
    payload: rentalId
})