import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import { rentals } from './rentals';
import { comments } from './comments';
import { promotions } from './promotions';
import { activities } from './activities';
import { favorites } from './favorites';
import { persistStore, persistCombineReducers } from 'redux-persist';
import storage from 'redux-persist/es/storage';

export const ConfigureStore = () => {

    const config = {
        key: 'root',
        storage,
        debug: true
    };

    const store = createStore(
        persistCombineReducers(config, {
            rentals,
            comments,
            promotions,
            activities,
            favorites
        }),
        applyMiddleware(thunk, logger)
    );

    const persistor = persistStore(store);
    return { persistor, store };
}