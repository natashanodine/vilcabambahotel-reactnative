import * as ActionTypes from './ActionTypes';

export const rentals = (state = {
    isLoading: true,
    errMess: null,
    rentals: []
}, action) => {

    switch (action.type) {
        case ActionTypes.ADD_RENTALS:
            return { ...state, isLoading: false, errMess: null, rentals: action.payload };

        case ActionTypes.RENTALS_LOADING:
            return { ...state, isLoading: true, errMess: null, rentals: [] };

        case ActionTypes.RENTALS_FAILED:
            return { ...state, isLoading: false, errMess: action.payload, rentals: [] };

        default:
            return state;
    }
}