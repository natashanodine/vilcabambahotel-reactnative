import React from 'react';
import { Text, ScrollView } from 'react-native';
import { Card, Button, Icon } from 'react-native-elements';
import * as Animatable from 'react-native-animatable';
import { MailComposer } from 'expo';

function RenderContactDetails() {


    const sendMail = () => {
        MailComposer.composeAsync({
            recipients: ['info@vilcabam-bahotel.com'],
            subject: 'Enquiry',
            body: 'To whom it may concern:',
        });
    }

    const contactTitle = 'Contact Information';
    const contactInfo = 'Yamburara Alto\n\nVilcabamba\n\nLoja\n\nTel: +593 991062762';
    return (
        <ScrollView>
            <Animatable.View animation="fadeInDown" duration={2000}>
                <Card
                    title={contactTitle}>
                    <Text style={{ margin: 10 }}>
                        {contactInfo}
                    </Text>
                    <Button
                        title=' Send Email'
                        buttonStyle={{ backgroundColor: '#35A5C4' }}
                        icon={<Icon name='envelope-o' type='font-awesome' color='white' />}
                        onPress={() => sendMail()}
                    />
                </Card>
            </Animatable.View>
        </ScrollView >
    );
}

export default RenderContactDetails;