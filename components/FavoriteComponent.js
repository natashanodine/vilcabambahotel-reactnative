import React, { Component } from 'react';
import { FlatList, Text, View, Alert } from 'react-native';
import { ListItem } from 'react-native-elements';
import { connect } from 'react-redux';
import { baseUrl } from '../shared/baseUrl';
import { Loading } from './LoadingComponent';
import Swipeout from 'react-native-swipeout';
import { deleteFavorite } from '../redux/ActionCreators';
import * as Animatable from 'react-native-animatable';

const mapStateToProps = state => {
    return {
        rentals: state.rentals,
        favorites: state.favorites
    }
}


const mapDispatchToProps = dispatch => ({
    deleteFavorite: (rentalId) => dispatch(deleteFavorite(rentalId))
})

class Favorites extends Component {

    static navigationOptions = {
        title: 'My Favorites'
    }

    render() {
        const { navigate } = this.props.navigation;

        const renderMenuItem = ({ item, index }) => {

            const rightButton = [
                {
                    text: 'Delete',
                    type: 'delete',
                    onPress: () => {
                        Alert.alert(
                            'Delete Favorite?',
                            'Are you sure you wish to delete the favorite rental ' + item.name + '?',
                            [
                                {
                                    text: 'Cancel',
                                    onPress: () => console.log(item.name + ' Not Deleted'),
                                    style: 'cancel'
                                },
                                {
                                    text: 'Ok',
                                    onPress: () => this.props.deleteFavorite(item.id)
                                }
                            ],
                            { cancelable: false }
                        );
                    }
                }
            ]

            return (
                <Swipeout right={rightButton} autoClose={true}>
                    <Animatable.View animation="fadeInRightBig" duration={2000}>
                        <ListItem
                            key={index}
                            title={item.name}
                            /*subtitle={item.description}*/
                            leftAvatar={{ source: { uri: baseUrl + item.image } }}
                            hideChevron={true}
                            onPress={() => navigate('Rentaldetail', { rentalId: item.id })} />
                    </Animatable.View>
                </Swipeout>
            );
        }

        if (this.props.rentals.isLoading) {
            return (
                <Loading />
            );
        } else if (this.props.rentals.errMess) {
            return (
                <View>
                    <Text>{this.props.rentals.errMess}</Text>
                </View>
            );
        } else {
            return (
                <FlatList
                    data={this.props.rentals.rentals.filter(rental => this.props.favorites.some(el => el === rental.id))}
                    renderItem={renderMenuItem}
                    keyExtractor={item => item.id.toString()} />
            );
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Favorites);