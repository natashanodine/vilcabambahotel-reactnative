import React, { Component } from 'react';
import { ScrollView, Text, FlatList } from 'react-native';
import { Card, ListItem } from 'react-native-elements';
import { connect } from 'react-redux';
import { baseUrl } from '../shared/baseUrl';
import { Loading } from './LoadingComponent';
import * as Animatable from 'react-native-animatable';

const mapStateToProps = state => {
    return {
        activities: state.activities
    }
}


class Ecoactivity extends Component {

    static navigationOptions = {
        title: 'Ecoactivities'
    };

    render() {
        const renderActivityItem = ({ item, index }) => {
            return (
                <ListItem
                    roundAvatar
                    key={index}
                    title={item.name}
                    subtitle={item.description}
                    subtitleNumberOfLines={15}
                    hideChevron={true}
                    leftAvatar={{ source: { uri: baseUrl + item.image } }}
                />
            );
        };

        if (this.props.activities.isLoading) {
            return (
                <ScrollView>
                    <Card
                        title='Corporate Activitieship'>
                        <Loading />
                    </Card>
                </ScrollView>
            );
        } else if (this.props.activities.errMess) {
            return (
                <ScrollView>
                    <Animatable.View animation="fadeInDown" duration={2000}>
                        <Card
                            title='Ecoactivities'>
                            <Text>{this.props.activities.errMess}</Text>
                        </Card>
                    </Animatable.View>
                </ScrollView>
            );
        } else {
            return (
                <ScrollView>
                    <Animatable.View animation="fadeInDown" duration={2000} >
                        <Card
                            title='Ecoactivities'>
                            <FlatList
                                data={this.props.activities.activities}
                                renderItem={renderActivityItem}
                                keyExtractor={item => item.id.toString()}
                            />
                        </Card>
                    </Animatable.View>
                </ScrollView>
            );
        }
    }
}

export default connect(mapStateToProps)(Ecoactivity);