import React, { Component } from 'react';
import { FlatList, Text, View } from 'react-native';
import { Tile } from 'react-native-elements';
import { connect } from 'react-redux';
import { baseUrl } from '../shared/baseUrl';
import { Loading } from './LoadingComponent';
import * as Animatable from 'react-native-animatable';

const mapStateToProps = state => {
    return {
        rentals: state.rentals
    }
}

class Menu extends Component {

    static navigationOptions = {
        title: 'Menu'
    };

    render() {
        const renderMenuItem = ({ item, index }) => {
            return (
                <Animatable.View animation="fadeInRightBig" duration={2000}>
                    <Tile
                        key={index}
                        title={item.name}
                        featured
                        onPress={() => navigate('Rentaldetail', { rentalId: item.id })}
                        imageSrc={{ uri: baseUrl + item.image }}
                    />
                </Animatable.View>
            );
        }
        const { navigate } = this.props.navigation;
        if (this.props.rentals.isLoading) {
            return (
                <Loading />
            );
        } else if (this.props.rentals.errMess) {
            return (
                <View>
                    <Text>{this.props.rentals.errMess}</Text>
                </View>
            );
        } else {
            return (
                <FlatList
                    data={this.props.rentals.rentals}
                    renderItem={renderMenuItem}
                    keyExtractor={item => item.id.toString()}
                />
            );
        }
    }
}

export default connect(mapStateToProps)(Menu);