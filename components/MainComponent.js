import React, { Component } from 'react';
import Home from './HomeComponent';
import Ecoactivity from './EcoactivityComponent';
import Menu from './MenuComponent';
import Rentaldetail from './RentalDetailComponent';
import Reservation from './ReservationComponent'
import Favorites from './FavoriteComponent';
import Contact from './ContactComponent';
import Login from './LoginComponent';
import { View, Platform, Image, Text, StyleSheet, ScrollView, NetInfo, ToastAndroid } from 'react-native';
import { createStackNavigator, createDrawerNavigator, DrawerItems, SafeAreaView } from 'react-navigation';
import { Icon } from 'react-native-elements'
import { connect } from 'react-redux';
import { fetchRentals, fetchComments, fetchPromos, fetchActivities } from '../redux/ActionCreators';

const mapStateToProps = state => {
    return {
        rentals: state.rentals,
        comments: state.comments,
        promotions: state.promotions,
        activities: state.activities
    }
}

const mapDispatchToProps = dispatch => ({
    fetchRentals: () => dispatch(fetchRentals()),
    fetchComments: () => dispatch(fetchComments()),
    fetchPromos: () => dispatch(fetchPromos()),
    fetchActivities: () => dispatch(fetchActivities()),
})


const LoginNavigator = createStackNavigator({
    Login: { screen: Login }
}, {
        navigationOptions: ({ navigation }) => ({
            headerStyle: {
                backgroundColor: '#82b6c5'
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                color: '#fff'
            },
            headerLeft: <Icon name='menu' size={24} color='white' onPress={() => navigation.toggleDrawer()} />
        })
    });


const HomeNavigator = createStackNavigator({
    Home: { screen: Home }
}, {
        navigationOptions: ({ navigation }) => ({
            headerStyle: {
                backgroundColor: '#82b6c5'
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                color: '#fff'
            },
            headerLeft: <Icon name='menu' size={24} color='white' onPress={() => navigation.toggleDrawer()} />
        })
    });

const EcoactivityNavigator = createStackNavigator({
    Ecoactivity: { screen: Ecoactivity }
}, {
        navigationOptions: ({ navigation }) => ({
            headerStyle: {
                backgroundColor: '#82b6c5'
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                color: '#fff'
            },
            headerLeft: <Icon name='menu' size={24} color='white' onPress={() => navigation.toggleDrawer()} />
        })
    });

const MenuNavigator = createStackNavigator({
    Menu: {
        screen: Menu,
        navigationOptions: ({ navigation }) => ({
            headerLeft: <Icon name='menu' size={24} color='white' onPress={() => navigation.toggleDrawer()} />
        })
    },
    Rentaldetail: { screen: Rentaldetail }

}, {
        initialRouteName: 'Menu',
        navigationOptions: {
            headerStyle: {
                backgroundColor: '#82b6c5'
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                color: '#fff'
            }
        }
    });

const ContactNavigator = createStackNavigator({
    Contact: { screen: Contact }
}, {
        navigationOptions: ({ navigation }) => ({
            title: 'Contact Us',
            headerStyle: {
                backgroundColor: '#82b6c5'
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                color: '#fff'
            },
            headerLeft: <Icon name='menu' size={24} color='white' onPress={() => navigation.toggleDrawer()} />
        })
    });

const FavoritesNavigator = createStackNavigator({
    Favorites: { screen: Favorites }
}, {
        navigationOptions: ({ navigation }) => ({
            headerStyle: {
                backgroundColor: '#82b6c5'
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                color: '#fff'
            },
            headerLeft: <Icon name='menu' size={24} color='white' onPress={() => navigation.toggleDrawer()} />
        })
    });

const ReservationNavigator = createStackNavigator({
    Reservation: { screen: Reservation }
}, {
        navigationOptions: ({ navigation }) => ({
            headerStyle: {
                backgroundColor: '#82b6c5'
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                color: '#fff'
            },
            headerLeft: <Icon name='menu' size={24} color='white' onPress={() => navigation.toggleDrawer()} />
        })
    });

const CustomDrawerContentComponent = (props) => (
    <ScrollView>
        <SafeAreaView
            style={styles.container} forceInset={{ top: 'always', horizontal: 'never' }}>
            <View style={styles.drawerHeader}>
                <View style={{ flex: 1 }}>
                    <Image source={require('./images/logo.gif')} style={styles.drawerImage} />
                </View>
                <View style={{ flex: 2 }}>
                    <Text style={styles.drawerHeaderText}>Vilcabamba Hotel</Text>
                </View>
            </View>
            <DrawerItems {...props} />
        </SafeAreaView>
    </ScrollView>
);

const MainNavigator = createDrawerNavigator({
    Login: {
        screen: LoginNavigator,
        navigationOptions: {
            title: 'Login',
            drawerLabel: 'Login',
            drawerIcon: ({ tintColor }) => (
                <Icon name='sign-in' size={24} type='font-awesome' color={tintColor} />
            )
        }
    },
    Home: {
        screen: HomeNavigator,
        navigationOptions: {
            title: 'Home',
            drawerLabel: 'Home',
            drawerIcon: ({ tintColor }) => (
                <Icon name='home' size={24} type='font-awesome' color={tintColor} />
            )
        }
    },
    Ecoactivity: {
        screen: EcoactivityNavigator,
        navigationOptions: {
            title: 'Ecoactivities',
            drawerLabel: 'Ecoactivities',
            drawerIcon: ({ tintColor }) => (
                <Icon name='info-circle' size={24} type='font-awesome' color={tintColor} />
            )
        }
    },
    Menu: {
        screen: MenuNavigator,
        navigationOptions: {
            title: 'Menu',
            drawerLabel: 'Menu',
            drawerIcon: ({ tintColor }) => (
                <Icon name='list' size={24} type='font-awesome' color={tintColor} />
            )
        }
    },
    Contact: {
        screen: ContactNavigator,
        navigationOptions: {
            title: 'Contact Us',
            drawerLabel: 'Contact Us',
            drawerIcon: ({ tintColor }) => (
                <Icon name='address-card' size={22} type='font-awesome' color={tintColor} />
            )
        }
    },
    Favorites: {
        screen: FavoritesNavigator,
        navigationOptions: {
            title: 'My Favorites',
            drawerLabel: 'My Favorites',
            drawerIcon: ({ tintColor }) => (
                <Icon name='heart' size={24} type='font-awesome' color={tintColor} />
            )
        }
    },
    Reservation: {
        screen: ReservationNavigator,
        navigationOptions: {
            title: 'Reserve Room',
            drawerLabel: 'Reserve Room',
            drawerIcon: ({ tintColor }) => (
                <Icon name='address-book' size={24} type='font-awesome' color={tintColor} />
            )
        }
    }
}, {
        initialRouteName: 'Home',
        drawerBackgroundColor: '#82b6c5',
        contentComponent: CustomDrawerContentComponent
    })

class Main extends Component {

    componentDidMount() {
        this.props.fetchRentals();
        this.props.fetchComments();
        this.props.fetchPromos();
        this.props.fetchActivities();

        NetInfo.getConnectionInfo()
            .then((connectionInfo) => {
                ToastAndroid.show('Initial network connectivity type: ' +
                    connectionInfo.type + ', effective type: ' + connectionInfo.effectiveType,
                    ToastAndroid.LONG);
            });

        NetInfo.addEventListener('connectionChange', this.handleConnectivityChange);
    }

    componentWillUnmount() {
        NetInfo.removeEventListener('connectionChange', this.handleConnectivityChange);
    }

    handleConnectivityChange = (connectionInfo) => {
        switch (connectionInfo.type) {
            case 'none':
                ToastAndroid.show('You are now Offline!', ToastAndroid.LONG);
                break;
            case 'wifi':
                ToastAndroid.show('You are now connected to WiFi!', ToastAndroid.LONG);
                break;
            case 'cellular':
                ToastAndroid.show('You are now connected to Cellular!', ToastAndroid.LONG);
                break;
            case 'unknown':
                ToastAndroid.show('You now have an Unknown connection!', ToastAndroid.LONG);
                break;
            default:
        }
    }

    render() {
        return (
            <View style={{ flex: 1, paddingTop: Platform.OS === 'ios' ? 0 : Expo.Constants.statusBarHeight }} >
                <MainNavigator />
            </View>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    drawerHeader: {
        backgroundColor: '#82b6c5',
        height: 140,
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        flexDirection: 'row'
    },
    drawerHeaderText: {
        color: 'white',
        fontSize: 24,
        fontWeight: 'bold'
    },
    drawerImage: {
        margin: 10,
        width: 80,
        height: 60
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(Main);